--
-- PostgreSQL database dump
--

-- Dumped from database version 10.14
-- Dumped by pg_dump version 10.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accesstoken; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.accesstoken (
    id character varying NOT NULL,
    ttl integer,
    scopes text,
    created date,
    userid integer,
    deletedat timestamp without time zone
);


ALTER TABLE public.accesstoken OWNER TO postgres;

--
-- Name: core_customer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.core_customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000
    CACHE 1;


ALTER TABLE public.core_customer_id_seq OWNER TO postgres;

--
-- Name: core_customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.core_customer (
    id integer DEFAULT nextval('public.core_customer_id_seq'::regclass) NOT NULL,
    name character varying(64) NOT NULL,
    address character varying,
    email character varying(64),
    phone character varying(16)
);


ALTER TABLE public.core_customer OWNER TO postgres;

--
-- Name: core_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.core_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000
    CACHE 1;


ALTER TABLE public.core_user_id_seq OWNER TO postgres;

--
-- Name: core_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.core_user (
    id integer DEFAULT nextval('public.core_user_id_seq'::regclass) NOT NULL,
    name character varying(64),
    username character varying(64),
    password character varying,
    email character varying(64),
    emailverified character varying(255),
    realm character varying(255),
    verificationtoken character varying(255)
);


ALTER TABLE public.core_user OWNER TO postgres;

--
-- Data for Name: accesstoken; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.accesstoken (id, ttl, scopes, created, userid, deletedat) FROM stdin;
P5GPyBymXqEGPTrZwymsFpEBGeNaXkgC3kQZ56Gow7JAmdMGtOb0Q5nZnODLU0ZF	1209600	\N	2021-04-08	2	\N
\.


--
-- Data for Name: core_customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.core_customer (id, name, address, email, phone) FROM stdin;
1	customer 1	address customer 1	customer1@mail.com	12345678910
3	Customer 3	address customer3	customer3@mail.com	1234567890
2	Customer 2	address customer2	customer2@mail.com	123123123123
\.


--
-- Data for Name: core_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.core_user (id, name, username, password, email, emailverified, realm, verificationtoken) FROM stdin;
1	ilham	ilham	P@ssw0rd	ilham@mail.com	\N	\N	\N
2	test1	test1	$2a$10$fGBjDBW8XaFPNT67KHX3EOPa0NrKa2eW9zzXJnD.ScD5EuLF.BS3W	test1@mail.com	false		\N
\.


--
-- Name: core_customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.core_customer_id_seq', 3, true);


--
-- Name: core_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.core_user_id_seq', 2, true);


--
-- Name: accesstoken accesstoken_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accesstoken
    ADD CONSTRAINT accesstoken_pkey PRIMARY KEY (id);


--
-- Name: core_customer core_customer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.core_customer
    ADD CONSTRAINT core_customer_pkey PRIMARY KEY (id);


--
-- Name: core_user core_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.core_user
    ADD CONSTRAINT core_user_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--


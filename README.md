# CRUD Customer-ILHAM TEST

Konfigurasi DB :
- nama DB = etos_challenge
- Konfig DB di server/datasource.json
- restore DB etos_challenge.sql yang ada pada root direktori

Loopback (localhost:3000) :
- npm i
- npm start

Angular (localhost:4200) :
- cd client
- npm i
- ng serve

Kredensial login :
- Username : admin
- Password : P@ssw0rd

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreCustomer } from 'src/app/models/core-customer';
@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  private readonly URL = 'http://localhost:3000/api/core_customers';


  constructor(private http: HttpClient) { }

  getCoreCustomers(){
    return this.http.get(this.URL);
  }

  postCoreCustomer(core_customer: CoreCustomer){
    return this.http.post(this.URL, core_customer)
  }

  updateCoreCustomer(core_customer: CoreCustomer, id: string){
    return this.http.put(this.URL+`/${id}`, core_customer)
  }

  deleteCoreCustomer(id: string){
    return this.http.delete(this.URL+`/${id}`)
  }

  getOneCoreCustomer(id: string){
    return this.http.get(this.URL+`/${id}`)
  }
}

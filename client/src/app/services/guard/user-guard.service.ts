import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationEnd, NavigationStart, NavigationError } from '@angular/router';
// import { Observable } from 'rxjs';
import { CoreUserService } from 'src/app/services/core-user.service';
import { CredentialService } from 'src/app/services/credential.service';
// import { Cookie } from 'ng2-cookies';

@Injectable()
export class UserGuard implements CanActivate {
    dataLogin = this.coreUserService.isAuthenticated();

    constructor(private router: Router, private coreUserService: CoreUserService, private CredentialService: CredentialService) {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.CredentialService.setPath(event['url']);
            }
        });
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        console.log({next:next.url[0].path, dataLogin:this.dataLogin, loggedIg:this.coreUserService.isAuthenticated()})
        if (this.coreUserService.isAuthenticated()) {
            if(next.url[0].path=='login'){
                this.router.navigateByUrl('/')
                return false;
            }else{
                return true;
            }
        } else {
            if(next.url[0].path=='login'){
                return true;
            }else{
                this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
                return false
            }
        }
    }
}

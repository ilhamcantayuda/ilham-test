import { Injectable } from "@angular/core";
import { CoreUserService } from "src/app/services/core-user.service";
// import { Cookie } from "ng2-cookies";

@Injectable()
export class CredentialService {
  dataLogin = this.CoreUserService.isAuthenticated();
  dataToken = this.CoreUserService["auth"].getToken();
  dataUser = this.dataToken.user;
  path: any;
  
  constructor(
    private CoreUserService: CoreUserService,
  ) {}

  login() {
    return this.dataLogin;
  }

  token() {
    return this.dataToken;
  }

  user() {
    return this.dataUser;
  }

  setDataUser() {
    this.dataToken = this.CoreUserService["auth"].getToken();
    this.dataUser = this.dataToken.user;
  }

  getDataUser() {
    return this.dataUser;
  }

  getDataToken() {
    return this.dataUser;
  }

  getPath() {
    return this.path;
  }

  setPath(path) {
    this.path = path;
  }
}

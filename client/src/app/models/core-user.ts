/* tslint:disable */
declare var Object: any;
  export interface CoreUserInterface {
    "id"?: number;
    "name"?: string;
    "username"?: string;
    "password"?: string;
    "email"?: string;
    "emailVerified"?: boolean;
    "realm"?: string;
    "deletedAt"?: Date;
    accessTokens?: any[];
  }
  
  export class CoreUser implements CoreUserInterface {
    "id"?: number;
    "name"?: string;
    "username"?: string;
    "password"?: string;
    "email"?: string;
    "emailVerified"?: boolean;
    "realm"?: string;
    "deletedAt"?: Date;
    accessTokens: any[];
    constructor(data?: CoreUserInterface) {
      Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `CoreUser`.
     */
    public static getModelName() {
      return "CoreUser";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of CoreUser for dynamic purposes.
    **/
    public static factory(data: CoreUserInterface): CoreUser{
      return new CoreUser(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
      return {
        name: 'CoreUser',
        plural: 'CoreUsers',
        path: 'CoreUsers',
        idName: 'id',
        properties: {
          "id": {
            name: 'id',
            type: 'number'
          },
          "name": {
            name: 'name',
            type: 'string'
          },
          "username": {
            name: 'username',
            type: 'string'
          },
          "password": {
            name: 'password',
            type: 'string'
          },
          "email": {
            name: 'email',
            type: 'string'
          },
          "emailVerified": {
            name: 'emailVerified',
            type: 'boolean'
          },
          "realm": {
            name: 'realm',
            type: 'string'
          },
          "deletedAt": {
            name: 'deletedAt',
            type: 'Date',
            default: new Date(0)
          },
        },
        relations: {
          accessTokens: {
            name: 'accessTokens',
            type: 'any[]',
            model: '',
            relationType: 'hasMany',
                    keyFrom: 'id',
            keyTo: 'userId'
          },
        }
      }
    }
  }
  
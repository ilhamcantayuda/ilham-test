/* tslint:disable */
import { Injectable } from '@angular/core';
import { CoreUser } from 'src/app/models/core-user';
import { CoreCustomer } from 'src/app/models/core-customer';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    CoreUser: CoreUser,
    CoreCustomer: CoreCustomer,
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}

export class CoreCustomer {
    id?: string;
    name: string;
    address: string;
    email: string;
    phone: string
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './containers/login/login.component';
import { MainCoreCustomerComponent } from './containers/main-core-customer/main-core-customer.component';
import { MainComponent } from './containers/main/main.component';
import { NewCoreCustomerComponent } from './containers/new-core-customer/new-core-customer.component';
import { UserGuard } from 'src/app/services/guard/user-guard.service';

const routes: Routes = [

  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: MainComponent,
    canActivate: [UserGuard]
  },
  {
    path: 'customers',
    component: MainCoreCustomerComponent,
    canActivate: [UserGuard]
  },
  {
    path: 'customers/new',
    component: NewCoreCustomerComponent,
    canActivate: [UserGuard]
  },
  {
    path: 'customers/edit/:id',
    component: NewCoreCustomerComponent,
    canActivate: [UserGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [UserGuard]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

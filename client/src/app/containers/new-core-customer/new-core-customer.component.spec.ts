import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCoreCustomerComponent } from './new-core-customer.component';

describe('NewCoreCustomerComponent', () => {
  let component: NewCoreCustomerComponent;
  let fixture: ComponentFixture<NewCoreCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewCoreCustomerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCoreCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

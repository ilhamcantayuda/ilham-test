import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-core-customer',
  templateUrl: './new-core-customer.component.html',
  styleUrls: ['./new-core-customer.component.scss']
})
export class NewCoreCustomerComponent implements OnInit {
  id: any;

  constructor(private route: ActivatedRoute) {
    this.id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    console.log(this.id)
  }

}

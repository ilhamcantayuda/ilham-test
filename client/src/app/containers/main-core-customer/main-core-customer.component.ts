import { Component, OnInit } from '@angular/core';
import { CoreCustomer } from 'src/app/models/core-customer';
import { DatabaseService } from 'src/app/services/database.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-main-core-customer',
  templateUrl: './main-core-customer.component.html',
  styleUrls: ['./main-core-customer.component.scss']
})
export class MainCoreCustomerComponent implements OnInit {
  core_customers: CoreCustomer[]=[];
  filter_core_customers: CoreCustomer[]=[];
  headElements = ['id', 'name', 'email', 'phone', 'address'];
  isClick: string;
  countClick: number;

  constructor(private databaseService:DatabaseService) {}

  ngOnInit(): void {
    this.getCoreCustomers();
    this.countClick=1;
  }

  getCoreCustomers(){
    this.databaseService.getCoreCustomers()
    .subscribe((core_customers:CoreCustomer[])=>{
      this.core_customers = core_customers;
      this.filter_core_customers = core_customers;
      console.log({core_customers:this.core_customers})
    }, err => {
      console.error(err);
    })
  }

  delete(core_customer: CoreCustomer) {
    const id = core_customer.id;

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.databaseService.deleteCoreCustomer(id)
          .subscribe(res => {
            console.log(res);
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            location.reload();
          }, err => {
            console.error(err);
            Swal.fire(
              'Failure!',
              'Your file could not delete.',
              'error'
            )
          })
      }
    })
  }

  onSearchChange(searchValue: string): void {
    console.log({search:searchValue});
    if(searchValue.length==0){
      this.filter_core_customers = this.core_customers;
    }else{
      this.filter_core_customers = this.core_customers.filter(
        customer => 
        customer.name.includes(searchValue) ||
        customer.email.includes(searchValue) ||
        customer.phone.includes(searchValue) ||
        customer.address.includes(searchValue)
      );
      console.log({afterFilter:this.filter_core_customers})
    }
  }

  onSort(event) {
    this.isClick = event;
    console.log({eventSort:event})
    if(event=='name'){
      if(this.isClick=='name'){
        this.countClick++;
        if(this.countClick % 2 == 0){
          this.filter_core_customers.sort((a, b) => (a.name > b.name) ? -1 : 1)
        }else{
          this.filter_core_customers.sort((a, b) => (a.name > b.name) ? 1 : -1)
        }
      }else{
        this.countClick=1;
      }
    }else if(event=='email'){
      if(this.isClick=='email'){
        this.countClick++;
        if(this.countClick % 2 == 0){
          this.filter_core_customers.sort((a, b) => (a.email > b.email) ? -1 : 1)
        }else{
          this.filter_core_customers.sort((a, b) => (a.email > b.email) ? 1 : -1)
        }
      }else{
        this.countClick=1;
      }
    }else if(event=='id'){
      if(this.isClick=='id'){
        this.countClick++;
        if(this.countClick % 2 == 0){
          this.filter_core_customers.sort((a, b) => (a.id > b.id) ? -1 : 1)
        }else{
          this.filter_core_customers.sort((a, b) => (a.id > b.id) ? 1 : -1)
        }
      }else{
        this.countClick=1;
      }
    }else if(event=='phone'){
      if(this.isClick=='phone'){
        this.countClick++;
        if(this.countClick % 2 == 0){
          this.filter_core_customers.sort((a, b) => (a.phone > b.phone) ? -1 : 1)
        }else{
          this.filter_core_customers.sort((a, b) => (a.phone > b.phone) ? 1 : -1)
        }
      }else{
        this.countClick=1;
      }
    }else if(event=='address'){
      if(this.isClick=='address'){
        this.countClick++;
        if(this.countClick % 2 == 0){
          this.filter_core_customers.sort((a, b) => (a.address > b.address) ? -1 : 1)
        }else{
          this.filter_core_customers.sort((a, b) => (a.address > b.address) ? 1 : -1)
        }
      }else{
        this.countClick=1;
      }
    }
  }
}
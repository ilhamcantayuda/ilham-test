import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainCoreCustomerComponent } from './main-core-customer.component';

describe('MainCoreCustomerComponent', () => {
  let component: MainCoreCustomerComponent;
  let fixture: ComponentFixture<MainCoreCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainCoreCustomerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainCoreCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

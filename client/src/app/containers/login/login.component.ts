import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CoreUserService } from 'src/app/services/core-user.service';
import Swal from 'sweetalert2';
import { CredentialService } from 'src/app/services/credential.service';
// import { Cookie } from 'ng2-cookies';
import { NgForm } from '@angular/forms';
import { CoreUser } from 'src/app/models/core-user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  core_user: CoreUser = new CoreUser;
  // email: any;
  // password: any;
  // credentials = { email: this.email, password: this.password };

  constructor(private router: Router, private coreUserService: CoreUserService, private credentialService: CredentialService) { }

  toLowerCase(x: string) {
    this.core_user.email = x.toLowerCase();
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (re.test(email)) {
      return true
    }
  }

  onSubmit(form: NgForm) {
    if (form.invalid) {
      Swal.fire(
        'Oops...',
        'Credential not found !',
        'error'
      );
    } else {
      // console.log({formLogin:form.value, formLogin2: this.core_user})
      var pref: any;
      var include: any;
      if (this.validateEmail(this.core_user.email.toLowerCase())) {
        pref = { email: this.core_user.email.toLowerCase(), password: this.core_user.password }
      } else {
        pref = { username: this.core_user.email.toLowerCase(), password: this.core_user.password }
      }

      include = [{
        relation: "user"
      }];

      this.coreUserService.login(pref).subscribe((info) => {
        this.credentialService.setDataUser();
        this.router.navigateByUrl("/")
      }, (error) => {
        Swal.fire(
          'Oops...',
          'Credential not found !',
          'error'
        );
      });
    }
  }

  ngOnInit(): void {
  }

}

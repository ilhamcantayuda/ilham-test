import { Component, OnInit } from '@angular/core';
import { CredentialService } from 'src/app/services/credential.service';
import { CoreUserService } from 'src/app/services/core-user.service'
import { CoreUser } from 'src/app/models/core-user';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  core_user: CoreUser = new CoreUser;
  statusLogin = this.credentialService.login();

  constructor(private router: Router, private credentialService: CredentialService, private coreUserService: CoreUserService) { }

  ngOnInit(): void {
    console.log({
      dataUser:this.credentialService.user(),
      dataLogin:this.credentialService.login(),
      dataToken:this.credentialService.token(),
    })
    this.core_user = this.credentialService.user()
  }

  logout() {
    // localStorage.removeItem('access_token');
    this.coreUserService.logout().subscribe((info) => {
      console.log({infoLogout:info})
      this.statusLogin = false;
      this.router.navigateByUrl('/login');
    }, (error) => {
      console.error({errorLogout:error})
      Swal.fire(
        'Failed',
        error,
        'error'
      );
      // alert(error.message);
    });
  }

}

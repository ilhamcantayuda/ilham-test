import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CoreCustomer } from 'src/app/models/core-customer';
import { DatabaseService } from 'src/app/services/database.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-core-customer',
  templateUrl: './form-core-customer.component.html',
  styleUrls: ['./form-core-customer.component.scss']
})
export class FormCoreCustomerComponent implements OnInit {
  core_customer: CoreCustomer = new CoreCustomer;
  @Input() id: string;
  constructor(public databaseService: DatabaseService, private router: Router) { }

  ngOnInit(): void {
    if(this.id){
      this.getOne();
    }
  }

  send(form: NgForm){
    if(form.invalid){
      Swal.fire(
        'Failed',
        'Form Invalid !',
        'error'
      );
    }else{
      if(this.id){
        this.edit(form)
      }else{
        this.post(form)
      }
    }
  }

  edit(form: NgForm){
    this.databaseService.updateCoreCustomer(form.value, this.id)
    .subscribe(res => {
      console.log(res);
      Swal.fire(
        'Success',
        'Customer updated !',
        'success'
      );
      this.router.navigateByUrl('/customers');
    }, err => {
      console.error(err);
      Swal.fire(
        'Failed',
        'Update customer failed !',
        'error'
      );
    })
  }

  post(form: NgForm){
    this.databaseService.postCoreCustomer(form.value)
    .subscribe( res => {
      console.log(res);
      Swal.fire(
        'Success',
        'Customer created !',
        'success'
      );
      this.router.navigateByUrl('/customers');
    }, err => {
      console.error(err);
      Swal.fire(
        'Failed',
        'Create customer failed !',
        'error'
      );
    })
  }

  getOne(){
    this.databaseService.getOneCoreCustomer(this.id)
    .subscribe((core_customer: CoreCustomer)=>{
      console.log(core_customer)
      this.core_customer = core_customer;
    }, err => {
      console.error(err)
      Swal.fire(
        'Failed',
        'Fetch customer failed !',
        'error'
      );
    })
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCoreCustomerComponent } from './form-core-customer.component';

describe('FormCoreCustomerComponent', () => {
  let component: FormCoreCustomerComponent;
  let fixture: ComponentFixture<FormCoreCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormCoreCustomerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCoreCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './containers/main/main.component';
import { NavComponent } from './components/nav/nav.component';
import { NewCoreCustomerComponent } from './containers/new-core-customer/new-core-customer.component';
import { FormCoreCustomerComponent } from './components/form-core-customer/form-core-customer.component';
import { MainCoreCustomerComponent } from './containers/main-core-customer/main-core-customer.component';
import { LoginComponent } from './containers/login/login.component';
import { CoreUserService } from './services/core-user.service';
import { CredentialService } from './services/credential.service';
import { SocketConnection } from './services/sockets/socket.connection';
import { SocketDriver } from './services/sockets/socket.driver';
import { SDKModels } from './models/custom/sdk-model';
import { LoopBackAuth } from './services/core/auth.service';
import { InternalStorage } from './storages/storage.swap';
import { UserGuard } from './services/guard/user-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    NewCoreCustomerComponent,
    FormCoreCustomerComponent,
    MainCoreCustomerComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule   
  ],
  providers: [
    CoreUserService,
    CredentialService,
    SocketConnection,
    SocketDriver,
    SDKModels,
    LoopBackAuth,
    InternalStorage,
    UserGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
